package com.MultiThreadingPractice.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class Main {

	public static List<Integer> list = new ArrayList<Integer>();
	public static volatile boolean running = true;
	public Random random=new Random();
	public static final int MAX_LIST_SIZE=20;//Maximum size of list in producer buffer
	public static final int NUMBER_RANGE=100;//Range of numbers displayed in output will be upto 100
	
	public static void main(String[] args) {
		final Main main = new Main();
		Thread produces = new Thread(new Runnable() {

			@Override
			public void run() {
				main.produce();
			}

		});

		Thread consumes = new Thread(new Runnable() {
			@Override
			public void run() {
				main.consume();
			}

		});

		System.out.println("Hit enter to stop\n");
		produces.start();
		consumes.start();

		Scanner scan = new Scanner(System.in);
		scan.nextLine();
		running = false;
		System.out.println("The process has stopped.");

		try {
			produces.join();
			consumes.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void produce() {
		synchronized (this) {
			while (running) {
				if (list.size() < MAX_LIST_SIZE) {
					list.add(random.nextInt(NUMBER_RANGE));
					notify();
				} else {
					try {
						this.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void consume() {
		synchronized (this) {
			while (running) {
				if (!list.isEmpty()) {
					System.out.println(list.remove(0)+" has been removed.");
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					notify();
				}

				else {
					try {
						this.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	
}
